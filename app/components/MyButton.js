/**
 * @flow
 */
import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import * as Colors from '../modules/theme/colors';

type Props = {
  text: string,
  type?: any,
  style?: any,
  onPress?: function,
};

type State = {
  text: string,
  type: any,
  style: any,
  onPress: function,
};

export default class MyButton extends Component<Props, State> {
  static defaultProps: Object;

  constructor(props: Props) {
    super(props);
    this.state = {
      text: props.text,
      style: props.style,
      type: props.type,
      onPress: props.onPress,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState){
    return {
      text: nextProps.text,
      style: nextProps.style,
      type: nextProps.type,
      onPress: nextProps.onPress,
    };
 }

  render() {
    const {
      text, type, onPress, style
    } = this.state;

    return (
      <TouchableOpacity 
      style={[{
        backgroundColor: type ? Colors.secondary : Colors.primary,
        borderWidth: 1, 
        borderRadius: 5, 
        borderColor: type ? Colors.primary : Colors.secondary, 
        width: '80%', 
        borderRadius: 5, 
        padding: 10, 
        marginBottom: 8
      }, style]}
       onPress={() => onPress()}>  
        <Text style={{
          color:type ? Colors.primary : Colors.readable, 
          textAlign: 'center', 
          fontWeight: '700'}}>
          {text}
        </Text>
      </TouchableOpacity> 
    );
  }
}

// DefaultProps
MyButton.defaultProps = {
  type: false,
  style: {},
  onPress: () => {},
};
