import Geolocation from 'react-native-geolocation-service';
import { GCM_API } from 'react-native-dotenv'
import Course from '../modules/model/Course';
import CourseDetails from '../modules/model/CourseDetails';
import { PermissionsAndroid, Platform } from 'react-native';
import I18n from '../i18n/i18n';
import AlertUtils from '../modules/utils/AlertUtils';

class LocalisationUtils {

    static API_KEY = GCM_API

    static async requestPermission() {
        if(Platform.OS === 'ios') {
            Geolocation.requestAuthorization();
        } else if( Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        'title': 'Cabito',
                        'message': I18n.t("Permissions.Localisation")
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("You can use the location")
                } else {
                    console.log("location permission denied")
                }
            } catch (err) {
                AlertUtils.alertOK(I18n.t("Error.Localisation"), I18n.t("Error.LocalisationMessage"))
            }
        }
    }

    static getLocalisation(onDone, onError) {
        Geolocation.getCurrentPosition(
            (position) => {
                // console.log(position);
                this.getAddressFromCoordinate({
                    lat: position.coords.latitude,
                    long: position.coords.longitude
                }, onDone, onError);
            },
            (error) => {
                AlertUtils.alertOK(I18n.t("Error.Localisation"), I18n.t("Error.LocalisationMessage"))
                onError(error);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }

    static getAddressFromCoordinate(coord: { lat: any, long: any }, onDone, onError) {
        const latlng = `${coord.lat},${coord.long}`

        fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${this.API_KEY}`)
            .then(response => {
                if (response.status / 100 < 4)
                    return response.json()
                else
                    onError(I18n.t('errChapterUnavailable') + " (" + response.status + ")")
            })
            .then(json => {
                if (json && json.results) {
                    // console.log(json.results[0].formatted_address)
                    onDone(json.results[0].formatted_address);
                }
                else {
                    onDone(I18n.t("Carte.AddressUkn"));
                }
            })
            .catch(error => {
                onError(error);
            });
    }

    static getDirection(start, end, onDone, onError, mode = 'driving') {
        fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${start}&destination=${end}&key=${this.API_KEY}&mode=${mode}`)
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.routes.length) {
                    const legs = responseJson.routes[0].legs[0]
                    //, legs.end_address, legs.end_location
                    const course = new Course(legs.start_address, legs.end_address)
                    const infos = new CourseDetails(legs.duration.text, legs.duration.value, legs.distance.text, legs.distance.value)
                    onDone(this.decode(responseJson.routes[0].overview_polyline.points), course, infos);
                }
                else {
                    onError("No Path");
                }
            })
            .catch(error => {
                onError(error);
            });
    }

    static decode(t, e) {
        for (var n, o, u = 0, l = 0, r = 0, d = [], h = 0, i = 0, a = null, c = Math.pow(10, e || 5); u < t.length;) 
        { 
            a = null, h = 0, i = 0; 
            do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; 
            while (a >= 32); 
            n = 1 & i ? ~(i >> 1) : i >> 1, h = i = 0; 
            do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; 
            while (a >= 32); 
            o = 1 & i ? ~(i >> 1) : i >> 1, l += n, r += o, d.push([l / c, r / c]) 
        } 
        return d = d.map(function (t) { return { latitude: t[0], longitude: t[1] } })
    }
}

export { LocalisationUtils };