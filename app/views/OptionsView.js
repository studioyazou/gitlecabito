/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import I18n from '../i18n/i18n';
import * as Colors from '../modules/theme/colors'
import Icon from 'react-native-vector-icons/FontAwesome';
import MyButton from '../components/MyButton';
import StorageUtils from '../modules/utils/StorageUtils';
import NavigationUtils from '../modules/utils/NavigationUtils';

type Props = { navigation: NavigationActions };

type State = {
  fav: any,
  modal: boolean,
};

export default class OptionsView extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const { params } = this.props.navigation.state || {};
    this.state = {
      fav: {},
      modal: params.modal || false,
    };

    StorageUtils.getDefault().then(res => {
      if( !Object.keys(res).length) { 
        res = {passenger: 1, luggage: 1, car: 0}
        StorageUtils.saveDefault(res);
      }
      this.setState({
        fav: res,
      })
    })
  }

  static navigationOptions = {
    header: null,
  };
  
  // ================================//
  // =========== Actions ============//
  // ================================//

  saveSettings() {
    const { fav } = this.state;

    StorageUtils.saveDefault(fav).then(() => {
      NavigationUtils.goBack(this)
    });
  }

  render() {
    const { fav, modal } = this.state;

    const modalExplanation = modal ? (
      <View>
        <Text style={{fontSize: 14, marginBottom: 16, textAlign:'center'}}> 
          {I18n.t("Settings.Explanation")}
        </Text>
        <View style={{marginHorizontal:'5%', backgroundColor: Colors.separator, height:2, marginBottom: 16}} />
      </View>
    ) : null

    const modalButton = modal ? null : (
      <MyButton
      text={I18n.t("back")}
      type={true}
      style={{width: '90%'}}
      onPress={() => NavigationUtils.goBack(this)}
    />);

    return (
      <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={[styles.container, {paddingHorizontal: 20}]}>
        <Text style={{fontSize: 25, fontWeight:'500', marginBottom: 20}}>
          {I18n.t('Settings.Title')}
        </Text>         
        <View style={{marginHorizontal:'5%', backgroundColor: Colors.separator, height:2, width:'50%', marginBottom: 16}} />
        {modalExplanation}

        <Text style={{fontSize: 16, marginBottom: 20}}>
          {I18n.t('Settings.NbPassenger')}
        </Text> 
        <View style={{flexDirection: 'row', marginBottom: 20}}>
          <TouchableOpacity onPress={() => {fav.passenger = 1; this.setState({fav})}}>  
            <Icon name="user" style={{marginRight: 20}} size={50} color={fav.passenger === 1 ? Colors.primary : Colors.black} backgroundColor='transparent'/>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {fav.passenger = 2; this.setState({fav})}}>  
            <Icon name="users" size={50} color={fav.passenger === 2 ? Colors.primary : Colors.black} backgroundColor='transparent'/>
          </TouchableOpacity>
        </View>

        <Text style={{fontSize: 16, marginBottom: 20}}>
          {I18n.t('Settings.NbLuggage')}
        </Text> 
        <View style={{flexDirection: 'row', marginBottom: 20}}>
          <TouchableOpacity onPress={() => {fav.luggage = !fav.luggage; this.setState({fav})}}>  
            <Icon name="suitcase" size={50} color={fav.luggage ? Colors.primary : Colors.black} backgroundColor='transparent'/>
          </TouchableOpacity>
        </View>

        <Text style={{fontSize: 16, marginBottom: 20}}>
          {I18n.t('Settings.Car')}
        </Text> 
        <View style={{flexDirection: 'row', marginBottom: 20}}>
          <MyButton
            text={I18n.t("Settings.Eco")}
            style={{width: '45%'}}
            type={fav.car ? true : false}
            onPress={() => {fav.car = 0; this.setState({fav})}} 
          />
          <MyButton
            text={I18n.t("Settings.Luxe")}
            style={{width: '45%'}}
            type={fav.car ? false : true}
            onPress={() => {fav.car = 1; this.setState({fav})}} 
          />
        </View>

        <MyButton
          text={I18n.t("save")}
          style={{width: '90%'}}
          onPress={() => this.saveSettings()}
        />
        {modalButton}

      </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.secondary,
  },
});
