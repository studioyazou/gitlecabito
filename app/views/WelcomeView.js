/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  TextInput,
  LayoutAnimation,
  TouchableOpacity,
  Platform,
  UIManager,
  Text,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import I18n from '../i18n/i18n';
import * as Regex from '../modules/utils/regex';
import * as Colors from '../modules/theme/colors';
import * as Images from '../modules/theme/images';
import Icon from 'react-native-vector-icons/FontAwesome';
import MyButton from '../components/MyButton';
import Course from '../modules/model/Course';
import StorageUtils from '../modules/utils/StorageUtils';
import NavigationUtils from '../modules/utils/NavigationUtils';
import { LocalisationUtils } from '../services/LocalisationUtils';
import AlertUtils from '../modules/utils/AlertUtils';

const pjson = require('../../package.json');

type Props = { navigation: NavigationActions };

type State = {
  address: string | null,
  addressValid: boolean,
  finish: string | null,
  finishValid: boolean,
  step: any,
  errorAddress: any,
  errorFinish: any,
};

const STEPS = {
  STEP_START: 0,
  STEP_END: 1,
};

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

export default class WelcomeView extends  Component<Props, State>  {

  static navigationOptions = {
    title: I18n.t('Welcome'),
    header: null,
  };

  constructor(props: Props) {
    super(props);
    // const { params } = this.props.navigation.state;
    this.state = {
      address: null,
      addressValid: false,
      errorMessage: '',
      finish: null,
      finishValid: false,
      errorAddress: '',
      errorFinish: '',
      step: STEPS.STEP_START,
    };

    LocalisationUtils.requestPermission();

    StorageUtils.getDefault().then(res => {
      if( !Object.keys(res).length) { 
        NavigationUtils.navigate(this, "Options", {modal:true})
      }
    })
  }

  componentDidMount() {
    SplashScreen.hide()
  }

  // ================================//
  // =========== Process ============//
  // ================================//

  checkAndNextStep() {
    let { addressValid, address, finishValid, finish, step } = this.state;
    LayoutAnimation.easeInEaseOut();
    if(step === STEPS.STEP_START) {
      this.setState({
        step: STEPS.STEP_END,
      }, () => {
        this.finishInput.focus()
      })
    }
    else if( step === STEPS.STEP_END ) {
      addressValid = addressValid && address && address.length > 0;
      finishValid = finishValid && finish && finish.length > 0;
      const update = {};
      if( !addressValid ) {
        update.addressValid=false;
        update.address='';
        update.errorAddress=I18n.t("Welcome.StartError");
      }
      if( !finishValid) {
        update.finishValid=false;
        update.finish='';
        update.errorFinish=I18n.t("Welcome.FinishError");
      }

      this.setState(update);

      if( !Object.keys(update).length) {
        if( this.addressInput.isFocused() ) this.addressInput.blur()
        if( this.finishInput.isFocused() ) this.finishInput.blur()
        NavigationUtils.navigate(this, "Map", {course: new Course(address, finish, {})});
      }
    }
  }

  // ==================================//
  // =========== TextInput ============//
  // ==================================//

  onChangeText(text: any) {
    LayoutAnimation.easeInEaseOut();
    if( this.addressInput.isFocused() ) {
      this.setState({
        address: text,
        addressValid: !Regex.regexAddress.test(text),
        errorAddress: '',
      });
    }
    else {
      this.setState({
        finish: text,
        finishValid: !Regex.regexAddress.test(text),
        errorFinish: '',
      });
    }
  }

  // =====================================//
  // =========== Localisation ============//
  // =====================================//

  getLocalisation() {
    LocalisationUtils.getLocalisation((address) => {
      this.setState({
        address,
        addressValid: !Regex.regexAddress.test(address),
        errorAddress: '',
      })
    }, (err) => {                
      AlertUtils.alertOK(I18n.t("Error.Localisation"), I18n.t("Error.LocalisationMessage"))
      // console.log("err", err)
    });
  }

  // ===================================//
  // =========== Definition ============//
  // ===================================//

  addressInput: any;
  finishInput: any;

  render() {
    const { address, finish, addressValid, finishValid, errorAddress, errorFinish, step } = this.state;
    const start = step == STEPS.STEP_START;

    const geolocView = (
      <TouchableOpacity onPress={() => this.getLocalisation()}>  
      <Icon name="map-marker" size={start ? 100 : 25} color={Colors.primary} backgroundColor='transparent'/>
    </TouchableOpacity> 
    )

    const errorAddressView = errorAddress ? <Text style={styles.errorText}>{errorAddress}</Text> : null
    const errorFinishView = errorFinish ? <Text style={styles.errorText}>{errorFinish}</Text> : null

    const endTitle = start ? null : (
        <Text style={styles.instructions}>
        {I18n.t('Welcome.FinishLoc')}
      </Text>)
    const endForm = start ? null : (
      <TextInput
        ref={((el) => {
          this.finishInput = el;
        })}
        style={[styles.searchInput, {borderColor: finishValid || finish === null ? Colors.primary : Colors.error}]}
        textContentType="addressCity"
        onChangeText={text => this.onChangeText(text)}
        value={finish}
        maxLength={100}
        autoCapitalize="none"
        autoCorrect={true}
        returnKeyType='done' 
        placeholder={I18n.t('Welcome.PlaceHolderAddress')}
        placeholderTextColor={Colors.white}
        clearButtonMode="while-editing"
      />
    )

    return (
      <KeyboardAwareScrollView contentContainerStyle={styles.container} alwaysBounceVertical={false} behavior="padding">
        <Image
          testID="logo"
          style={styles.logo}
          resizeMode="contain"
          source={start ? Images.appLogo : Images.appLogoEnd}
          onError={e => console.log(e)}
        />
        <Text style={styles.instructions}>
          {I18n.t('Welcome.StartLoc')}
        </Text>
        {geolocView}
        <TextInput
          ref={((el) => {
            this.addressInput = el;
          })}
          style={[styles.searchInput, {borderColor: addressValid || address === null ? Colors.primary : Colors.error}]}
          textContentType="addressCity"
          onChangeText={text => this.onChangeText(text)}
          value={address}
          maxLength={100}
          autoCapitalize="none"
          autoCorrect={true}
          returnKeyType='done' 
          placeholder={I18n.t('Welcome.PlaceHolderAddress')}
          placeholderTextColor={Colors.white}
          clearButtonMode="while-editing"
        />
        {errorAddressView}
        {endTitle}
        {endForm}
        {errorFinishView}
        <MyButton
          text={I18n.t("Settings.Title")}
          type={true}
          style={{marginTop:20}}
          onPress={() => NavigationUtils.navigate(this, "Options")}
        />

        <MyButton
          text={I18n.t(start ? 'continue' : 'valid')}
          onPress={() => this.checkAndNextStep()}
        />

        {/* <Text style={{fontSize: 14}}>V{pjson.version}</Text> */}
       </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.secondary,
  },
  instructions: {
    textAlign: 'center',
    fontSize: 16,
    color: Colors.text,
    marginBottom: 5,
  },
  errorText: {
    fontSize: 16,
    textAlign: 'center',
    color: Colors.error,
    marginTop: -8,
    marginBottom: 16,
  },
  logo: {
    backgroundColor: 'transparent',
    width: '25%',
    height: '25%',
  },
  searchInput: {
    marginVertical: 20,
    textAlign: 'center',
    fontSize: 17,
    borderWidth: 1,
    borderRadius: 5,
    padding: 8,
    width: '80%',
    color: Colors.white,
    backgroundColor: Colors.secondary 
  },
});
