/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
} from 'react-native';
import I18n from '../i18n/i18n';
import * as Colors from '../modules/theme/colors'
import Icon from 'react-native-vector-icons/FontAwesome';
import Course from '../modules/model/Course';
import CourseDetails from '../modules/model/CourseDetails';
import MapView, {PROVIDER_GOOGLE, Marker} from "react-native-maps";
import MyButton from '../components/MyButton';
import NavigationUtils from '../modules/utils/NavigationUtils';
import { LocalisationUtils } from '../services/LocalisationUtils';

type Props = { navigation: NavigationActions };

type State = {
  course : Course,
  coords: any,
  loading: boolean,
  infos: CourseDetails,
}

export default class CarteView extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const {params} = this.props.navigation.state || {  };
    // console.log("params", params);
    this.state = {
      course: params.course,
      coords: null, 
      loading: true,
      infos: {},
    };

    LocalisationUtils.getDirection(params.course.start, params.course.end, (coords, course, details) => {
      // console.log(coords, details)
      this.setState({
        coords,
        course, 
        infos: details,
        loading: false,
      })
    }, (err) => {
      // console.log('direction err', err);
      this.setState({
        loading: false,
      })
    });

  }
  static navigationOptions = { header: null };

  render() {
    const {course, infos, coords, loading} = this.state;

    let startMark = null;
    let endMark = null;
    let paths = null;
    let booking = null;

    let region = {
      latitude: 48.8805146,
      longitude: 2.3032342,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }

    if( coords && coords.length > 0 ) {

      region = {
        latitude : (coords[coords.length-1].latitude + coords[0].latitude) / 2,
        longitude : (coords[coords.length-1].longitude + coords[0].longitude) / 2,
        latitudeDelta: Math.abs(coords[coords.length-1].latitude - coords[0].latitude)*1.15,
        longitudeDelta: Math.abs(coords[coords.length-1].longitude - coords[0].longitude)*1.15,
      }

      startMark = (
        <Marker
          coordinate={{
          latitude: coords[0].latitude,
          longitude: coords[0].longitude,
        }}
        pinColor={Colors.error}
      />);

    endMark = (
        <Marker
          coordinate={{
          latitude: coords[coords.length-1].latitude,
          longitude: coords[coords.length-1].longitude,
        }}
        pinColor={Colors.primary}
      />);

      paths =  (
        <MapView.Polyline
        coordinates={[
            ...this.state.coords,
        ]}
        strokeWidth={4}
      />
      )

      booking = (
        <MyButton
          text={`${I18n.t('Carte.Reserve')}\n${infos.time} (${infos.distance}) - ${infos.price}€`}
          style={{width: '100%'}}
          onPress={() => {NavigationUtils.navigate(this, "Completion")}}
        />)
    }
    else if (!loading) {
      booking = (
      <MyButton
        text={I18n.t("Carte.TripUnavailable")}
        style={{width: '100%', backgroundColor:"red"}}
        onPress={() => {NavigationUtils.goBack(this)}}
      />)
    }

    return (
      <SafeAreaView style={styles.container}>
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          region={region}
          showsUserLocation={true}
          followUserLocation={true}
          loadingEnabled={true}
        >
          {startMark}
          {paths}
          {endMark}
          </MapView>
        <View
          style={{position: "absolute", bottom: 20, width: "80%"}}>
          <View
            style={{
              borderColor: Colors.black,
              borderWidth: 0.5,
              borderRadius: 5,
              marginBottom: 8,
              paddingVertical: 8,
              paddingHorizontal: 16,
              backgroundColor:Colors.whiteTransparent,
            }}>
            <View style={{ flexGrow: 1, justifyContent: "center" }}>
              <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: 'center' }}>
                <Icon
                  name="flag"
                  size={20}
                  color={Colors.error}
                  backgroundColor="transparent"
                />
                <Text style={[styles.centered, { flex: 1 }]}>{course.start}</Text>
              </View>
            </View>
            <View style={{marginHorizontal:'5%', backgroundColor: Colors.black, height:0.5}} />
            <View style={{ flexGrow: 1, marginTop: 8, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent:'center' }}>
                <Icon
                  name="flag"
                  size={20}
                  color={Colors.primary}
                  backgroundColor="transparent"
                />
                <Text style={[styles.centered, { flex: 1 }]}>{course.end}</Text>
              </View>
            </View>
          </View>

          {booking}
          <MyButton
            text={I18n.t("back")}
            type={true}
            style={{width: '100%'}}
            onPress={() => NavigationUtils.goBack(this)}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.secondary,
  },
  centered: {
    textAlign:'center',
    color: Colors.text,
    marginBottom: 5,
    marginLeft: 8,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
