/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  SafeAreaView,
  View
} from 'react-native';
import LottieView from 'lottie-react-native';
import I18n from '../i18n/i18n';
import * as Colors from '../modules/theme/colors'
import Icon from 'react-native-vector-icons/FontAwesome';
import MyButton from '../components/MyButton';
import NavigationUtils from '../modules/utils/NavigationUtils';

type Props = { navigation: NavigationActions };

type State = {
}

export default class CompletionView extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    // const {params} = this.props.navigation.state || {  };
    this.state = {
    };
  }
  static navigationOptions = { header: null };
  
  render() {
    return (
      <SafeAreaView style={{flex:1, alignItems:'center'}}>
        <Text style={{textAlign:'center', marginTop: 30, color:Colors.black, fontSize: 20, fontWeight: 'bold'}}>{I18n.t("Resut.Incoming")}</Text>
        <LottieView source={require('../assets/animation/car.json')} autoPlay loop />
        <MyButton
          text={I18n.t("back")}
          style={{position:'absolute', bottom:20}}
          onPress={() => NavigationUtils.goBack(this)}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
});
