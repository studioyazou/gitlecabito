export const primary = '#2c59f5';
export const secondary = '#FFFFFF';
export const readable = "#FFFFFF"
export const text = '#51565a';
export const separator = '#DDDDDDCC';

export const whiteTransparent = '#FFFFFFCC';
export const black = '#1D1D1D'
export const error = '#f52c59'
