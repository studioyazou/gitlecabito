/**
 * @flow
 */
const appLogo = require('../../assets/images/logo.png');
const appLogoEnd = require('../../assets/images/logoEnd.png');

export {
  appLogo, appLogoEnd
};
