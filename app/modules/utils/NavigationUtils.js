import { NavigationActions } from 'react-navigation';

export default class NavigationUtils {

  static async navigate(nav, routeName, params = {}) {
    nav.props.navigation.navigate(routeName, params);
  }

  static async goBack(nav) {
    nav.props.navigation.dispatch(NavigationActions.back({}));
  }

}