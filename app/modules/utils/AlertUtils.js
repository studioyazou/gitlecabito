import { Alert } from 'react-native';
import I18n from '../../i18n/i18n';

export default class AlertUtils {

  static async alertOK(title, msg, onOk = null) {
    Alert.alert(
      title,
      msg,
      [
        {
          text: I18n.t('ok'),
         onPress: () => onOk ? onOk() : {}
        },
      ],
      {cancelable: false},
    );
  }

  static async alertOKCancel(title, msg, onOk = null, onCancel = null) {
    Alert.alert(
      title,
      msg,
      [
        {
          text: I18n.t('ok'), 
          onPress: () => onOk ? onOk() : {}
        },
        {
          text: I18n.t('cancel'),
          onPress: () => {
            if(onCancel) onCancel();
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  }

}