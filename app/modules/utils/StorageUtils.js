import {
  AsyncStorage,
} from 'react-native';

const keyDefault = '@cabito:default';

export default class StorageUtils {

  //================================//
  //=========== Divers =============//
  //================================//

  static async getDefault() {
    try {
      var favs = await AsyncStorage.getItem(keyDefault);

      if (favs === null)
        favs = {};
      else
        favs = JSON.parse(favs);

      return favs;
    } catch (error) {
      throw(error);
    }
  }

  static async resetDefault() {
    try {
      await AsyncStorage.setItem(keyDefault, JSON.stringify({}));
      return true;
    } catch (error) {
      throw(error);
    }
  }

  static async saveDefault(options) {
    try {
      await AsyncStorage.setItem(keyDefault, JSON.stringify(options));
      return true;
    } catch (error) {
      throw(error);
    }
  }
}