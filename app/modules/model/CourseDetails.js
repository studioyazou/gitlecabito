function CourseDetails(time, timeRaw, distance, distanceRaw, priceBase= 3, priceKm=1.88) {
  this.time = time;
  this.timeRaw = timeRaw;
  this.distance = distance;
  this.distanceRaw = distanceRaw;
  this.price = (priceBase + (distanceRaw/1000 * priceKm)).toFixed(2);
  return this;
}

module.exports = CourseDetails;