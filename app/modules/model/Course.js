function Course(start, end, options = {}) {
  this.start = start;
  this.end = end;
  this.options = options;
  return this;
}

module.exports = Course;