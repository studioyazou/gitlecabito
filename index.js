/**
 * @format
 */

import { AppRegistry, View, StatusBar } from 'react-native';
import {name as appName} from './app.json';
import { StackNavigator, NavigationActions } from 'react-navigation';

import WelcomeView from './app/views/WelcomeView';
import OptionsView from './app/views/OptionsView';
import CarteView from './app/views/CarteView';
import CompletionView from './app/views/CompletionView';

console.disableYellowBox = true;

const App = StackNavigator({
    Home: { screen: WelcomeView },
    Options: { screen: OptionsView },
    Map: { screen: CarteView },
    Completion: { screen: CompletionView },
});

const AppContent = () =>
  <View style={{flex: 1, backgroundColor:'white'}}>
    <StatusBar barStyle="light-content" backgroundColor={'transparent'} translucent/>
   <App />
 </View>;

const navigateOnce = (getStateForAction) => (action, state) => {
  const {type, routeName} = action;
  return (
    state &&
    type === NavigationActions.NAVIGATE &&
    routeName === state.routes[state.routes.length - 1].routeName
  ) ? null : getStateForAction(action, state);
  // you might want to replace 'null' with 'state' if you're using redux (see comments below)
};

App.router.getStateForAction = navigateOnce(App.router.getStateForAction);

AppRegistry.registerComponent(appName, () => App);
